# clase importada para creacion de modelo
from django.db import models

# clase creada para el modelo de producto
class Product(models.Model):
    #atributos del producto, que van a representar las columnas de la tabla
    Codigo = models.AutoField(primary_key=True)
    Producto = models.CharField('NameProduct', max_length = 15)
    Tipo = models.CharField(max_length = 15)
    Precio = models.IntegerField(default=0)
    Cantidad = models.IntegerField(default=0)