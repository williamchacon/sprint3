from django.apps import AppConfig


class AuthappclearConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'authAppclear'
